\documentclass[
    abstract=on,
    paper=a4,
    DIV=12,
    bibliography=totoc]{scrreprt}
\usepackage{libertine}
\usepackage[style=ieee,block=ragged]{biblatex}
\usepackage[hidelinks]{hyperref}
\usepackage{etoolbox}


\title{Social, Legal and Ethical Aspects of Computing: Media Analysis}
\author{%
    Jamie Atkin-Wasti\\
    Lucille L. Blumire\\
    Vittorio Filice\\
    Richard Leake\\
    Said Rahmani\\
    Ivan Syrovoiskii\\
}
\date{December 2018}

\renewcommand{\abstractname}{Introduction}

\addbibresource{citations.bib}

\makeatletter
\patchcmd{\scr@startchapter}{\if@openright\cleardoublepage\else\clearpage\fi}{}{}{}
\makeatother

\begin{document}

\maketitle

\begin{abstract}
This report discusses the social, ethical and legal themes from analysing Black Mirror Episode one and six in season three called ``Nosedive'' and ``Hated in the Nation''. We as a group identified the social, ethical, professional and legal aspects used in those specific episodes. Furthermore, we also were involved in identifying the negative and positive aspect of each factor, based on the episodes we analysed as a group. This report will also discuss how neuro-technologies featured in the show and will compare how similar technologies are impacting us in the real world.
 
Black Mirror is a humorous anthology series that looks at parts of current society, especially the connection between society and technology. Every episode shows a futuristic world which presents new innovations, some of which are powerful advancements and it also challenges the basic social, legal and ethical aspects of society. The added drama and the realistic scenes attempt to show us these challenges and question our perspective of possibilities within the technology field.

\begingroup
\let\clearpage\relax
\tableofcontents
\endgroup
\end{abstract}

\chapter{Hated in the Nation---Season 3 Episode 6}

In this episode, autonomous drone insects are a part of an artificial substitute bee developed by a company called Granular to neutralise a sudden colony collapse disorder in the population of Bees. Later on, its discovered that it's the reason such a large number of people losing their life's on the grounds that these ADIs are hacked by someone and it leads to someone's death. This is done through social media where the person with most targets receives a death vote.

As humans are living in a period of advanced technology, humans have an intimate connection with technology. Studies find that a considerable amount of our time within our lives is spent using technology whether for entertainment, academic research, social interactions or simply to find out information \cite{Ofcom24h} Technological progression has been a centrepiece of society in the last couple of decades, as it has been helping fields such as, science and medicine. However, with new technology come unanticipated outcomes because of the rapid expansion into a technologically based society. This idea is explored by the TV series, Black Mirror.

\section{Social}

The story opens by following a journalist called Jo Powers and providing the social commentary that twenty thousand people have signed a petition to have her lose her job as a result of an article she wrote about a disabled person. This highlights the very real social impact that a journalist can have, and the negative backlash, which is reinforced by her being insulted on the street, and a suspicious package arriving at her house. The package turned out to simply be cake with a hate message written on it, however, in an age where through social media people's addresses are becoming increasingly difficult to find it could easily have been a worse threat. Something akin to this happened in real life back in 2014, when a right-wing journalist Milo Yiannopoulos received a needle in the post over his commentary on the GamerGate controversy \cite{MiloNeedle}. This demonstrates the ease through which online social discourse---Jo receives many hate speech messages on Twitter---can translate into an actual real-world threat. It only requires one person to ``take it so far'' out of ever-increasing large numbers of people that are active in online social debates. The story progresses to follow a police investigation of the death of Jo Powers, with the constant noise of incoming tweets showing just how prevalent the hate messages were in Jo's life.

The protagonist, Blue, goes on to begin collating the hate messages directed at Jo, attempting to identify a killer in them. Her boss, Karin, comments that ``[Internet hate] drifts off like the weather. It is half hate. They do not mean it.'' She dismisses the idea, though in reality the hate propagated by the online disinhibition effect \cite{Suler2004} is often much higher than actual face to face hate, and online hate can be a good predictor of actual physical crimes \cite{MMller2017}.

Eventually, Jo's husband wakes up and presents the Alibi that Jo's injuries were self-inflicted. Seemingly as a result of pain in her head. They then follow up with the sender of the cake, and it turns out she is a primary school teacher who had previously tweeted ``\#DeathTo @JoPowersWriter''. A primary school teacher who presents herself as fairly likeable is not the type of person who would be expected to send death threats, which again brings up the ideas presented by the online disinhibition effect: behind social media, even if not anonymous, people can often act in ways they otherwise would not, changing their personality to one that exhibits more extreme views than in their day to day life.

It is eventually revealed that \#DeathTo is a ``hashtag game'' in which people insert the name of someone who is ``being an arsehole''. This again raises the more extreme language used online, jumping to a death threat as a casual act being propagated through collective ideas on social media.

It is revealed that the cybernetic bees used by the government to replace those that died to environmental changes are being controlled by the aggregation of the \#DeathTo hashtag, allowing the public to vote on who should be killed each day. The story moves to the next target, Clara Meades, who is shown to be under a great deal of mental distress as a result of the use of the hashtag, without even knowing its implications on her life. This highlights the psychological impacts of cyber-bullying, which have been found to be parallel to the psychological impacts of physical bullying \cite{Kowalski2013} and are therefore quite serious. A fact that is reinforced later by one of the interrogated suspects who talk about their history with online abuse, and how it eventually resulted in her self harming and attempting suicide: things far more common in those who are the targets of online harassment \cite{John2018}.

As public awareness of the hashtag grows, its numbers begin to explode exponentially. The episode presents arguments for its use, and questions the culpability of those who use it. An argument presented in the episode is that ``someone has to be at the top of the list, might as well make it someone who deserves it.'' This argument highlights the acceptance of online hate and the usage of the hashtag as an inevitability. It could be argued that the social media companies had a social responsibility to prevent the hashtags usage, however, preventing usage in this way---especially for hate services---can prove extremely difficult. A good example would be how Reddit  attempted banning one of its subreddits,  /r/fatpeoplehate, which progressed through at least 75 alternatives \cite{RedditFattening} which were all banned, and likely still exist in some capacity to this day.

Eventually, the killer releases his manifesto: ``Thanks to the technological revolution we have the power to rage an accuse, spout bile without consequence. Only by being forced to recognise the power technology grants us, to acknowledge individual responsibility\ldots''. Revealing the killers intent, to make the public acknowledge the destructive powers of social media. The killer's inspiration was his colleague who was the suspect that attempted suicide after social media harassment. This intent is revealed when it shows that the killer has collected the identity of every person to use the \#DeathTo hashtag with the intent to kill them. A feat that would make him the most successful serial killer in history.

The story ends by showing a large rally from the public, outside a hearing of those involved, demanding the truth of what happened. Indicating the government had not released the events that transpired despite the extremely large death count. Invoking their right to freedom of peaceful assembly.

\section{Legal}

Apart from raising a lot of social issues, Black Mirror also raises a lot of law-related problems.

The robot swarm pictured in Hated In the Nation is fully autonomous and relies on the Artificial Intelligence software for making decisions. This is somewhat similar to how autonomous cars operate---they use sensors to gather data for their surroundings and then use it for navigation. 

As was mentioned before, the drones were created to prevent an ecological catastrophe caused by the colony collapse disorder. Making the system autonomous and self-sustainable is very likely to be the only way to implement it on a scale that would cover the whole country---making it more centralised would probably involve building a data centre and would result in dramatically high costs, response latency and potential connection problems.

Relying on an AI to make decisions raises very serious legal concerns. First of all, just like with autonomous cars, it is not entirely clear who would be responsible if one of the robot bees causes an accident. A paper from Allen and Overy \cite{Allen2017} analyses the legal issues related to autonomous cars and asks the following questions: ``Who should be responsible for incidents caused by defects in the software interface between two cars or between a car and the road? \ldots\ Who should be held liable in the case of a cyber-attack on cars? Should the software manufacturer be strictly liable for defective software security that allowed third parties to hack into the car?'' All of these apply to autonomous drones as well because the legal responsibility for autonomous systems is very vague. This, combined with numerous unsolved ethical dilemmas, is one of the main reasons why some countries are very slow to update their laws to allow self-driving cars.

Another serious that will occur if such a system ever gets implemented in real life is privacy. In the episode, the honey bee drones had cameras which were secretly used to gather information for crime prevention purposes. Such a system would be highly illegal under current legislation, as it would contradict with the article eight of the Human Rights Act \cite{HumanRights1998}. While an argument can be made that surveillance can help with preventing crimes, it also can be severely abused. According to a 2016 article by Associated Press, ``Police officers across the country misuse confidential law enforcement databases to get information on romantic partners, business associates, neighbours, journalists and others for reasons that have nothing to do with daily police work'' \cite{APpolice}. Given that the drones in Black Mirror had inbuilt cameras, the nature of data they collect would be even more sensible and it would be even easier to misuse it for blackmailing or any other illegal activities.

Finally, it is not entirely clear where the law should stand regarding granting drones access to private property. While it can be assumed that open access lands would be accessible under the Countryside and Rights of Way Act 2000 \cite{CountrysideRights}, it is highly unlikely that drones would be allowed access to other kinds of private land like people's back gardens. This would mean that the developers would have to implement a system that would restrict the drones to only specific areas---which would require a very accurate no-fly map to ensure that bees only stay in public places. 

\section{Ethical}

After the detective analyses murders and looks at the comments on social media that use the hashtag, she realises that those people do not actually mean what they say, and what the hacker wants to emphasise is that  people would not say those things out loud but they would put it on social media, since typing behind a keyboard gives some people the courage to say those things. The hacker wants to make a point by showing that behaving unethically on social media has consequences just like doing it in person, which is why towards the end of the movie, all those people that used the hashtag \#DeathTo to kill people, get themselves killed by the artificial bees as a consequence of their unethical actions on social media.

Another interesting point about this episode is that it shows how much people depend on technology today, and this will increase in the future, as humans give up their data in exchange for services that make life easier and save time and money \cite{MentalFloss}. This is already happening, as many big companies like Facebook, Google, Amazon try to provide as many useful services as they can in exchange for data that will be used in different ways like targeted ads, research or even to sell data to other companies and although there are many benefits in using new technology, this data is often used to manipulate people and their opinions like in the Cambridge analytical case which exposed millions of Facebook accounts \cite{GuardianFacebook}. On one hand, technology is allowing us to do things that were thought impossible before, but like this film shows, the consequences of depending too much on technology can be catastrophic. The robot-bees have become so important for the ecosystem that humans became dependent on them, and when the bees get hacked, the consequences of being dependent on a technology like this are shown.

In order to avoid this, it is essential to discuss in an open, honest manner the ethical application of technology and the consequences of using technology in an unethical way.

\section{Professional}

What is a professional? 

In a general sense, a professional, is someone who is a member of a profession and a profession is a occupation that requires some level of training to be able to operate in a safe and effective manner. To ensure the quality of services offered by professionals there is a legal framework which applies to the regulation of professionals. 

Without going into too much detail, in the UK profession fall into a few categories based on; the body issuing the certification and the level of skill required to qualify. \cite{NARIC}\cite{ERWCPT}

If someone is found to be using their professional position irresponsibly they will usually face some kind of discipline. A degree can be revoked if is found that you cheated your way through it and you can be struck of the medical register for misbehaving as a doctor. Many professions will outline how one should conduct themselves in a code of conduct, or a code of ethics.

Throughout the episode there are numerous cases of potential misconduct, ranging from questionable behaviour from a journalist through to cases involving important government secrets.

The first issue is the article written by Jo Powers near the beginning. After a disability activist publicly immolates themselves to protest benefit cuts she writes a rather scathing piece about the event and ends up on the receiving end of the ire of the Internet.

While there isn't a regulatory body for certifying journalist (which as a concept opens up a whole new can of worms), the National Union of Journalists does provide a code of conduct \cite{NUJCoC} for their members which outlines how they should conduct themselves. Given the relatively short clip of the article shown in the episode there isn't a lot to work with. However, a few points stand out. Specifically points 4, 6 and 9. The article seems pretty heavy with opinion, does not seem at all concerned with the grief and distress of any potential family members and while it is not known if the article could be construed as encouraging hatred of the disabled it's certainly quite a touchy subject.

Next up is the way the members of the Metropolitan Police conduct themselves. Early on Members of the Met Police, DCI Karin Parke, Blue Coulson and DS Nick Shelton are introduced, who certainly do not comply with the standards of professional behaviour set down in the College of Policing Code of Ethics \cite{CoPCoE}. Including:

\begin{itemize}
    \item Several instances where Parke is not respectful, to colleagues or members of the public.
    \item Not keeping an open mind.
    \item Shelton takes part in the harassment of one of the victims via the \#DeathTo hashtag.
    \item Not being willing to get a higher authority involved when they really should---wanting the glory for solving it.
    \item Having access to data they should not have---Coulson has access to the private details of citizens, despite her not being supposed to.
\end{itemize}

Another group featured in the episode is the National Crime Agency (NCA), represented by the character Shaun Li. As opposed to regional police which only operate within a certain area, the NCA, operates on a national level. Its role is to tackle large scale organised crime and as such may be privy to more sensitive internal information than regional police are. 

The drones being used to replace bees can be utilised for surveillance. This is a fact that Li is not keen on getting out, becoming quite confrontational when the idea is suggested. Referring to the NCA code of conduct \cite{NCACoC} gives a somewhat mixed message, one one hand it specifies that officers should ``take responsibility for maintaining our levels of security clearance and informing the vetting team as soon as possible of any changes to our personal or work circumstances that might affect our security clearance'', however on the other hand it states they should also ``treat information with respect and only access or disclose it for a legitimate NCA purpose''. Given the circumstances, disclosing that information could potentially have saved a lot of lives.

The final person guilty of perhaps the worst example of misconduct is the Granular employee Garrett Scholes. Scholes is the one who utilises the backdoor in the ADI drones. Due to the sensitive nature of what the drones are capable of, the information would most likely be protected under the Official Secrets Act. Using legally protected information you are entrusted with to kill large numbers of people could most likely be construed as gross misconduct.


\chapter{Nosedive---Season 3 Episode 1}

Nosedive is set in a world where individuals can rate each other from one to five stars for each interaction they have, which can affect their socioeconomic status. Within the character's environment, public uses a technology which involves eye implants and mobile devices, everyone shares their daily activities and rates their interactions with others on a scale, which influences that individual's overall rating. One's current average can be seen by others and this impacts their socioeconomic status.

\section{Ethical}

The first ethical problem that we can notice in the episode is that people are treated differently depending on their score on the social network, so people with a low score are kept at the margin of society and are considered losers while others are seen as superstars. We can see this in our society too where people are treated differently depending on their follower number and other factors that make people more or less popular and accepted than others. The compulsive desire to impress others is seen in the movie when Lacy eats a cookie and buys coffee to post it on social media and afterwards she spits the cookie out and looks disgusted by the taste of coffee since she just had them to share a picture on the internet and not because she likes them.

We can also see Lacy looking at her mirror practising how to fake laugh at different levels, which shows how obsessed she is with the way others see her. She even has a ``therapist'' for her online profile that tells her what posts were more popular than others and other factors to help her get a higher rating on social media. These scenes put an emphasis on the need of the character to feel accepted even if she has to pretend to feel a certain way.

Another interesting scene is when she is in the elevator with another person, and instead of talking like normal people, the two only interact to talk about their social media feed which is the only point of the conversation, and shows how people consider social media more important than their real life. This is something that we can already see in today's society where everyone posts on social media only the best part of themselves to feel accepted and emotionally rewarded by a system that makes us conform to ideas of what is considered good and bad in our society. The system incentives narcissism, obsession and rewards conformity over self-expression and this movie only brings the concept of social media to an extreme to make the viewer understand the consequences of an inappropriate use of technology like social networks.

\section{Legal}

Nosedive also portraits a scenario which raises a lot of legal concerns. Unlike Hated in the Nation, a system somewhat similar to Nosedive is already implemented and is currently being used in China \cite{IndependentChina}. Similar to the score from Nosedive, the Chinese Social Credit Score controls what services one can get. It is reported that a low score can result in a ban for buying tickets (just like in Nosedive, where Lacie could not buy an airplane ticket because her score was too low), prevent children from being accepted to a good school and also stop people from getting a job. The latter is not exclusive for China---it is known that a poor credit score can prevent the employer from offering a job in the UK \cite{ExperianCredit}. 

Using a system like this opens a lot of room for discrimination. One of Lacie's coworkers, Chester, becomes a social exile due to his low score which is caused by a break up with his partner. The plot of the episode revolves around the fact that people with low scores are only allowed to live in certain areas, which is directly against the Freedom of Movement right. 

The only potential benefit of using a social score system is a potential stimulus to become ``a better person'' who makes a contribution to the society they live in. However, as we can see in Nosedive, relying on public opinion will only encourage people to abuse the system and exploit others to create a positive public image and improve their score. 

\chapter{Conclusion}

Throughout this essay, the themes surrounding Social, Legal, Ethical, and Professional issues in the two episodes of Black Mirror: Hated in the Nation, and Nosedive have been discussed. The impact of both mainstream media (through Jo Powers story) and of social media, on the mental health and wellbeing of society and those within it have been explored. The boldness and sense of power that comes from anonymity on the internet is a major theme throughout Hated in the Nation, and is an accurate depiction of the harassment campaigns, and general online hate that can have long-lasting, and sometimes deadly consequences.

Hated in the Nation also utilises very powerful Ethical themes throughout the episode. The viewer is left with many questions that they must ask themselves once the episode has ended. Do the benefits of the ADI's environmental impact outweigh the loss of privacy from the government mandated surveillance? Is it ethical to make an unethical choice for a better outcome (if \#DeathTo is going to kill somebody, is it our duty to make it someone who ``deserves'' it most? Similar to the classic trolley problem)? Should people be held accountable for things they say on the internet and if so, where do you draw the line and how do you punish those involved? 

A legal analysis has shown the ADI's to be violating privacy laws by entering and recording footage on people's private property, as well as violating the Human Rights Act. The legality of who is to blame for malfunctioning or unsecure autonomous drones seems to be a grey area. This is increasingly relevant as driverless cars and autonomous drones designed for war begin to become a reality.

The episode also shows multiple cases of unprofessional behaviour and even potential professional misconduct. These range from Jo Powers' hateful article, to taking part in the hashtag which both disclosed sensitive information that was important for the case at hand, as well as actively encouraging the death of someone on a public twitter account.

Nosedive explores the social and ethical themes of marginalised and (in Nosedive, socially) poor people and how they are often treated unfairly by those with power over them. Is it right to treat someone harshly because they are (according to a system) a bad person? Legally, the system (or at least parts of it) presented in the episode would be illegal under current legislation through Freedom of Movement.

Throughout the episode, Lacy loses most of the score she is put so much effort into increasing. However, halfway through the episode, she meets a truck driver who is rated very low but is living a happy and fulfilling life. This leads to the end of the episode, where she is able to freely speak her mind, arguing with someone in a neighbouring cell to her after she is placed in prison. Both she and the other person smile and laugh together, as they both realise that not having to perform and be someone you're not is cathartic. This is the first step to her righting her mistakes and being able to be who she really is, just like truck driver is.

In the future, this essay would most likely be able to more directly compare to real life, and allow us to observe whether the near future scenarios presented are actually the way these systems and technologies will develop. This is especially significant regarding self-driving cars, autonomous drone strikes, and China's social credit system.

If this essay were to be done again, organising group meetings where the whole group sit down and watch the episodes together, as well as more group discussions for each part of the essay would both be good changes. This would allow everyone's ideas and expertise to be used on all parts of the essay, resulting in a higher quality and more cohesive document.




%%===============================


\newpage
\printbibliography

\end{document}
